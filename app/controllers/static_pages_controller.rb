class StaticPagesController < ApplicationController
  def about
  end

  def help
  end
  
  def home
  end
  
  def blogs
    @posts= Micropost.all
  end
  
end
