Rails.application.routes.draw do

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  get 'users/new'
  get 'static_pages/about'
  get 'static_pages/home'
  get  '/help',    to: 'static_pages#help'
  get '/blog' , to: 'static_pages#blogs'
  # get 'static_pages/help'
 
  resources :users do
     resources :microposts
  end
  
  get  '/signup',  to: 'users#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  root "users#index"  
  
end
